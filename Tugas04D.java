/**
 * @(#)Tugas04D.java
 *
 *
 * @author 
 * @version 1.00 2023/3/28
 */

import java.util.Scanner;
public class Tugas04D {

    public static void main(String[] args) {
    	String nama, noPlg;
    	int pmkAir, BiayaPk1, BiayaPk2, total = 0;
    	
    	Scanner input03 = new Scanner(System.in);
    	
    	System.out.println("Perhitungan Biaya Pemakaian Air");
    	System.out.println("===========================================");
    	System.out.print("Nama                 : ");
    	nama = input03.nextLine();
    	System.out.print("No. Pelanggan        : ");
    	noPlg = input03.nextLine();
    	System.out.print("Pemakaian Air        : ");
    	pmkAir = input03.nextInt();
    	if (pmkAir <= 10) {
    		total = pmkAir * 1000;
    	} else if (pmkAir <= 20) {
    		BiayaPk1 = 10 * 1000;
    		BiayaPk2 = (pmkAir-10) * 2000;
    		total = BiayaPk1 + BiayaPk2;
    	} else {
    		BiayaPk1 = 10 * 1000;
    		BiayaPk2 = 10 * 2000;
    		total = BiayaPk1 + BiayaPk2 + ((pmkAir - 20) * 5000);
    	}
    	System.out.print("Biaya Pakai          : " + total);
    	System.out.println("\n===========================================");
    }
    
    
}